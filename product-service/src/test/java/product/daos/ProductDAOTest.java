package product.daos;

import model.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Product dao test.
 *
 * @author Dawid
 * @version 1
 */
@ExtendWith(SpringExtension.class)
@JdbcTest
@ComponentScan
class ProductDAOTest {
    @Autowired
    private ProductDAO dao;
    @Autowired
    @Qualifier("embeddedDB")
    private DataSource dataSource;
    @Autowired
    JdbcTemplate template;
    private Product product1, product2, product3, product4, product5;

    /**
     * Before each.
     */
    @BeforeEach
    void beforeEach() {
        product1 = new Product();
        product1.setValue(100);
        product1.setProductName("Test Product 1");
        product1.setCreditId(50);

        product2 = new Product();
        product2.setValue(200);
        product2.setProductName("Test Product 2");
        product2.setCreditId(50);

        product3 = new Product();
        product3.setValue(300);
        product3.setProductName("Test Product 3");
        product3.setCreditId(75);

        product4 = new Product();
        product4.setValue(400);
        product4.setProductName("Test Product 4");
        product4.setCreditId(100);

        product5 = new Product();
        product5.setValue(500);
        product5.setProductName("Test Product 5");
        product5.setCreditId(100);

    }

    /**
     * Save test.
     */
    @Test
    void saveTest() {
        assertNotNull(dao);
        Optional<Product> result = dao.save(product1);
        assertTrue(result.isPresent());
        assertEquals(result.get(), product1);
        assertNotNull(result.get().getId());
        assertTrue(result.get().getId() > 0);
        assertEquals(result.get(), product1);
        assertNotEquals(result.get(), product2);
        assertNotEquals(result.get(), product3);
        assertNotEquals(result.get(), product4);
        assertNotEquals(result.get(), product5);
    }

    /**
     * Gets all test.
     */
    @Test
    void getAllTest() {
        assertNotNull(dao);
        assertTrue(dao.save(product2).isPresent());
        assertTrue(dao.save(product4).isPresent());
        Optional<List<Product>> list = dao.getAll(new Integer[]{3000, product2.getCreditId(), 3001});
        assertTrue(list.isPresent());
        assertEquals(1, list.get().size());
        assertFalse(list.get().contains(product1));
        assertTrue(list.get().contains(product2));
        assertFalse(list.get().contains(product3));
        list = dao.getAll(new Integer[]{3000, 3001, product2.getCreditId(), 4580, 58955, product4.getCreditId(), 300, 302});
        assertTrue(list.isPresent());
        assertEquals(2, list.get().size());
        assertFalse(list.get().contains(product1));
        assertTrue(list.get().contains(product2));
        assertFalse(list.get().contains(product3));
        assertTrue(list.get().contains(product4));
        assertFalse(list.get().contains(product5));
    }

}
