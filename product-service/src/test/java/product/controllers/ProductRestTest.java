package product.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import model.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import product.daos.ProductDAO;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * The type Product rest test.
 *
 * @author Dawid
 * @version 1
 */
@WebMvcTest(ProductRest.class)
class ProductRestTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ProductDAO productDAO;

    private Map<Integer, Product> map;
    private int index;

    /**
     * Before each.
     */
    @BeforeEach
    void beforeEach() {
        map = new HashMap<>(5);
        index = 1;
    }

    /**
     * Product init.
     */
    void productInit() {
        Product product = new Product();
        product.setId(index++);
        product.setValue(100);
        product.setProductName("Test Product 1");
        product.setCreditId(50);
        map.put(product.getId(), product);

        product = new Product();
        product.setId(index++);
        product.setValue(200);
        product.setProductName("Test Product 2");
        product.setCreditId(50);
        map.put(product.getId(), product);

        product = new Product();
        product.setId(index++);
        product.setValue(300);
        product.setProductName("Test Product 3");
        product.setCreditId(75);
        map.put(product.getId(), product);

        product = new Product();
        product.setId(index++);
        product.setValue(400);
        product.setProductName("Test Product 4");
        product.setCreditId(100);
        map.put(product.getId(), product);

        product = new Product();
        product.setId(index++);
        product.setValue(500);
        product.setProductName("Test Product 5");
        product.setCreditId(100);
        map.put(product.getId(), product);

        when(productDAO.getAll(any(Integer[].class))).thenAnswer(invocationOnMock -> {
            Integer[] keys = invocationOnMock.getArgument(0);
            List<Product> list = new ArrayList<>();
            for (Integer key : keys) {
                list.addAll(map.values().stream().filter(p -> p.getCreditId().equals(key)).collect(Collectors.toList()));
            }
            return list.size() > 0 ? Optional.of(list) : Optional.empty();
        });
    }

    /**
     * Gets products.
     *
     * @throws Exception the exception
     */
    @Test
    void getProducts() throws Exception {
        productInit();
        mockMvc.perform(MockMvcRequestBuilders.put("/")
                .content("[2000,20001,200,234,443,345]").contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/")
                .content("[2000,50,200,234,443,345,75]").contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[1]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[2]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[3]").doesNotExist()).andReturn();
        ObjectMapper mapper = new JsonMapper();
        List<Product> list = Arrays.asList(mapper.readValue(mvcResult.getResponse().getContentAsString(), Product[].class));
        assertNotNull(list);
        assertTrue(list.contains(map.get(1)));
        assertTrue(list.contains(map.get(2)));
        assertTrue(list.contains(map.get(3)));
        assertFalse(list.contains(map.get(4)));
        assertFalse(list.contains(map.get(5)));
    }

    /**
     * Create product.
     *
     * @throws Exception the exception
     */
    @Test
    void createProduct() throws Exception {
        when(productDAO.save(any(Product.class))).thenAnswer(invocationOnMock -> {
            Product product = invocationOnMock.getArgument(0);
            product.setId(productDAO.save(product.getCreditId(), product.getProductName(), product.getValue()));
            map.put(product.getId(), product);
            return Optional.of(product);
        });

        when(productDAO.save(anyInt(), anyString(), anyInt())).thenReturn(index++);

        Product product1 = new Product();
        product1.setCreditId(500);
        product1.setProductName("Test Product 1");
        product1.setValue(1000);
        ObjectMapper mapper = new JsonMapper();
        mockMvc.perform(MockMvcRequestBuilders.post("/").content(mapper.writeValueAsString(product1))
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.creditId").value(product1.getCreditId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.productName").value(product1.getProductName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.value").value(product1.getValue()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber());

    }
}