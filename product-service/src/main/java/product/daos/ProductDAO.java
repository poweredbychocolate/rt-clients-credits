package product.daos;

import model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.sql.*;
import java.util.List;
import java.util.Optional;

/**
 * The Product Data Access Object.
 *
 * @author Dawid
 * @version 1
 */
@Service
public class ProductDAO {
    @Autowired
    private JdbcTemplate template;

    @PostConstruct
    private void onInit() {
        template.execute("CREATE SCHEMA IF NOT EXISTS ProductDB;");
        template.execute("CREATE TABLE IF NOT EXISTS ProductDB.Product( Id INT NOT NULL AUTO_INCREMENT, " +
                "CreditId INT NOT NULL, ProductName VARCHAR(500) NOT NULL, Value INT NOT NULL," +
                "PRIMARY KEY (Id))");

        System.out.println("[ProductDAO] Init Successfully (๑꧆◡꧆๑)");
    }

    /**
     * Save {@link Product}.
     *
     * @param product the product
     * @return the optional of saved object
     */
    public Optional<Product> save(Product product) {
        Integer i = save(product.getCreditId(), product.getProductName(), product.getValue());
        if (i != null) {
            product.setId(i);
            return Optional.of(product);
        }
        return Optional.empty();
    }

    /**
     * Insert product properties into database.
     *
     * @param creditId    the credit id
     * @param productName the product name
     * @param value       the value
     * @return saved Product Id
     */
    public Integer save(int creditId, String productName, int value) {
        final String insertSql = "INSERT INTO ProductDB.Product(CreditId,ProductName,Value) VALUES (?,?,?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        int c = template.update(connection -> {
            PreparedStatement statement = connection.prepareStatement(insertSql, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, creditId);
            statement.setString(2, productName);
            statement.setInt(3, value);
            return statement;
        }, keyHolder);
        return c > 0 && keyHolder.getKey() != null ? keyHolder.getKey().intValue() : null;
    }

    /**
     * Gets {@link Product} with id from creditIds list.
     *
     * @param creditIds the credit ids
     * @return the founded entries as {@link Optional}
     */
    public Optional<List<Product>> getAll(Integer[] creditIds) {
        StringBuilder stringBuilder = new StringBuilder("SELECT * FROM ProductDB.Product p WHERE p.CreditId IN(");
        for (Integer i : creditIds) {
            stringBuilder.append(i.toString());
            stringBuilder.append(", ");
        }
        stringBuilder.delete(stringBuilder.length() - 2, stringBuilder.length());
        stringBuilder.append(")");

        List<Product> list = template.query(stringBuilder.toString(),
//                connection -> {
//            PreparedStatement statement = connection.prepareStatement(getSql);
// (╯°□°）╯︵ ┻━┻     java.sql.SQLFeatureNotSupportedException: Not yet supported
//            Array array = connection.createArrayOf("INT", creditIds);
//            statement.setArray(1, array);
//            return statement;
//        },
                (resultSet, i) -> {
                    Product product = new Product();
                    product.setId(resultSet.getInt("Id"));
                    product.setCreditId(resultSet.getInt("CreditId"));
                    product.setValue(resultSet.getInt("Value"));
                    product.setProductName(resultSet.getString("ProductName"));
                    return product;
                });
        return list.size() > 0 ? Optional.of(list) : Optional.empty();
    }


}
