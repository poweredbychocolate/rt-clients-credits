package product.controllers;

import model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import product.daos.ProductDAO;

import java.util.List;
import java.util.Optional;

/**
 * Product rest controller.
 *
 * @author Dawid
 * @version 1
 */
@RestController
public class ProductRest {
    @Autowired
    private ProductDAO dao;
    private final ResponseEntity<List<Product>> NOT_FOUND_LIST = new ResponseEntity<>(HttpStatus.NOT_FOUND);
    private final ResponseEntity<Product> CONFLICT = new ResponseEntity<>(HttpStatus.CONFLICT);

    /**
     * Gets all {@link Product} from ids list .
     *
     * @param ids the ids list
     * @return the products
     */
    @PutMapping
    ResponseEntity<List<Product>> getProducts(@RequestBody Integer[] ids) {
        Optional<List<Product>> list = dao.getAll(ids);
        return list.map(products -> new ResponseEntity<>(products, HttpStatus.OK)).orElse(NOT_FOUND_LIST);
    }

    /**
     * Create new {@link Product}
     *
     * @param toSave Product to save
     * @return created Product or code 409 if save fail
     */
    @PostMapping
    synchronized ResponseEntity<Product> createProduct(@RequestBody Product toSave) {
        try {
            Optional<Product> tmp = dao.save(toSave);
            if (tmp.isPresent()) return new ResponseEntity<>(tmp.get(), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return CONFLICT;
    }
}
