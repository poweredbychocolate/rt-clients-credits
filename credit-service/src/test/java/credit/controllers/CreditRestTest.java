package credit.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import credit.daos.CreditDAO;
import credit.daos.RestDAO;
import credit.model.CreditInfo;
import model.Credit;
import model.Customer;
import model.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.annotation.PostConstruct;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@WebMvcTest(CreditRest.class)
class CreditRestTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private CreditDAO creditDAO;
    @MockBean
    private RestDAO restDAO;

    private Integer creditIndex = 1;
    private Integer productIndex = 1;
    private Integer customerIndex = 1;
    private Map<Integer, Credit> map;
    private Map<Integer, Product> productMap;
    private Map<Integer, Customer> customerMap;
    private ObjectMapper mapper = new JsonMapper();

    @PostConstruct
    public void onInit() {
        map = new HashMap<>();
        productMap = new HashMap<>();
        customerMap = new HashMap<>();
        when(creditDAO.getNewCreditId()).thenAnswer(invocationOnMock -> {
            return creditIndex++;
        });
        when(creditDAO.save(any(Credit.class))).thenAnswer(invocationOnMock -> {
            Credit credit = invocationOnMock.getArgument(0);
            map.put(credit.getId(), credit);
            return Optional.of(credit);
        });
        when(creditDAO.getAll()).thenAnswer(invocationOnMock ->
                map.values().isEmpty() ? Optional.empty() : Optional.of(new LinkedList<>(map.values())));

        when(restDAO.callCreate(any(Product.class))).thenAnswer(invocationOnMock -> {
            Product product = invocationOnMock.getArgument(0);
            product.setId(productIndex);
            productMap.put(productIndex++, product);
            return Optional.of(product);
        });
        when(restDAO.callCreate(any(Customer.class))).thenAnswer(invocationOnMock -> {
            Customer customer = invocationOnMock.getArgument(0);
            customer.setId(customerIndex);
            customerMap.put(customerIndex++, customer);
            return Optional.of(customer);
        });
        when(restDAO.callGetProducts(any(Integer[].class))).thenAnswer(invocationOnMock -> {
            Integer[] keys = invocationOnMock.getArgument(0);
            List<Product> list = new LinkedList<>();
            for (Integer k : keys) {
                list.addAll(productMap.values().stream().filter(product -> product.getCreditId().equals(k)).collect(Collectors.toList()));
            }
            return list.isEmpty() ? Optional.empty() : Optional.of(list);
        });
        when(restDAO.callGetCustomers(any(Integer[].class))).thenAnswer(invocationOnMock -> {
            Integer[] keys = invocationOnMock.getArgument(0);
            List<Customer> list = new LinkedList<>();
            for (Integer k : keys) {
                list.addAll(customerMap.values().stream().filter(product -> product.getCreditId().equals(k)).collect(Collectors.toList()));
            }
            return list.isEmpty() ? Optional.empty() : Optional.of(list);
        });

    }

    @Test
    void createProductTest() throws Exception {
        Credit credit = new Credit();
        credit.setCreditName("First Credit Test");
        credit.setId(creditDAO.getNewCreditId());
        assertNotNull(credit.getId());
        assertTrue(credit.getId() > 0);

        Product product = new Product();
        product.setValue(400000);
        product.setProductName("ND1 Nomad");
        product.setCreditId(credit.getId());

        Customer customer = new Customer();
        customer.setCreditId(credit.getId());
        customer.setPesel("12345678951");
        customer.setFirstName("Liara");
        customer.setSurname("T'Soni");

        CreditInfo creditInfo = new CreditInfo();

        mockMvc.perform(MockMvcRequestBuilders.post("/").content(mapper.writeValueAsString(creditInfo))
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
        creditInfo.setCredit(credit);
        mockMvc.perform(MockMvcRequestBuilders.post("/").content(mapper.writeValueAsString(creditInfo))
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
        creditInfo.setCredit(credit);
        creditInfo.setProduct(product);
        mockMvc.perform(MockMvcRequestBuilders.post("/").content(mapper.writeValueAsString(creditInfo))
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
        creditInfo.setCredit(credit);
        creditInfo.setProduct(null);
        creditInfo.setCustomer(customer);
        mockMvc.perform(MockMvcRequestBuilders.post("/").content(mapper.writeValueAsString(creditInfo))
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
        creditInfo.setCredit(credit);
        creditInfo.setProduct(product);
        creditInfo.setCustomer(customer);
        mockMvc.perform(MockMvcRequestBuilders.post("/").content(mapper.writeValueAsString(creditInfo))
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isNumber());
    }

    @Test
    void getCreditsTest() throws Exception {
        creditIndex = 1;
        customerIndex = 1;
        productIndex = 1;
        map.clear();
        productMap.clear();
        customerMap.clear();


        mockMvc.perform(MockMvcRequestBuilders.get("/").contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        Credit credit = new Credit();
        credit.setCreditName("First Credit Test");
        credit.setId(creditDAO.getNewCreditId());
        assertNotNull(credit.getId());
        assertTrue(credit.getId() > 0);

        Product product = new Product();
        product.setValue(400000);
        product.setProductName("ND1 Nomad");
        product.setCreditId(credit.getId());

        Customer customer = new Customer();
        customer.setCreditId(credit.getId());
        customer.setPesel("12345678951");
        customer.setFirstName("Liara");
        customer.setSurname("T'Soni");

        CreditInfo creditInfo1 = new CreditInfo(product, customer, credit);
        mockMvc.perform(MockMvcRequestBuilders.post("/").content(mapper.writeValueAsString(creditInfo1))
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());

        credit = new Credit();
        credit.setCreditName("Second Credit Test");
        credit.setId(creditDAO.getNewCreditId());
        assertNotNull(credit.getId());
        assertTrue(credit.getId() > 0);

        product = new Product();
        product.setValue(400000);
        product.setProductName("Star ship");
        product.setCreditId(credit.getId());

        customer = new Customer();
        customer.setCreditId(credit.getId());
        customer.setPesel("12345678951");
        customer.setFirstName("Tali'Zorah");
        customer.setSurname("nar Rayya");

        CreditInfo creditInfo2 = new CreditInfo(product, customer, credit);

        mockMvc.perform(MockMvcRequestBuilders.post("/").content(mapper.writeValueAsString(creditInfo2))
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
        credit = new Credit();
        credit.setCreditName("Another Credit Test");
        credit.setId(creditDAO.getNewCreditId());
        assertNotNull(credit.getId());
        assertTrue(credit.getId() > 0);

        product = new Product();
        product.setValue(100000000);
        product.setProductName("SSV Normandy SR-1");
        product.setCreditId(credit.getId());

        customer = new Customer();
        customer.setCreditId(credit.getId());
        customer.setPesel("12345678951");
        customer.setFirstName("Jane");
        customer.setSurname("Shepard");

        CreditInfo creditInfo3 = new CreditInfo(product, customer, credit);
        mockMvc.perform(MockMvcRequestBuilders.post("/").content(mapper.writeValueAsString(creditInfo3))
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());


        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/").contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[1]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[2]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[3]").doesNotExist())
                .andReturn();

        CreditInfo[] tmp = mapper.readValue(mvcResult.getResponse().getContentAsString(), CreditInfo[].class);
        assertEquals(3, tmp.length);
        assertNotEquals(tmp[0].getCredit().getId(), tmp[1].getCredit().getId());
        assertNotEquals(tmp[0].getCredit().getId(), tmp[2].getCredit().getId());
        assertNotEquals(tmp[1].getCredit().getId(), tmp[2].getCredit().getId());
        assertEquals(tmp[0].getCredit().getId(), tmp[0].getProduct().getCreditId());
        assertEquals(tmp[0].getCredit().getId(), tmp[0].getCustomer().getCreditId());
        assertEquals(tmp[0].getProduct().getCreditId(), tmp[0].getCustomer().getCreditId());
        assertNotEquals(tmp[0].getCredit().getId(), tmp[1].getProduct().getCreditId());
        assertNotEquals(tmp[0].getCredit().getId(), tmp[2].getProduct().getCreditId());
        assertNotEquals(tmp[0].getCredit().getId(), tmp[1].getCustomer().getCreditId());
        assertNotEquals(tmp[0].getCredit().getId(), tmp[2].getCustomer().getCreditId());
    }
}
