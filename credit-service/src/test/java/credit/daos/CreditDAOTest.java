package credit.daos;

import model.Credit;
import model.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.sql.DataSource;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@JdbcTest
@ComponentScan
class CreditDAOTest {
    @Autowired
    private CreditDAO dao;
    @Autowired
    @Qualifier("embeddedDB")
    private DataSource dataSource;
    @Autowired
    JdbcTemplate template;
    private Credit credit1, credit2, credit3, credit4, credit5;

    @BeforeEach
    void beforeEach() {
        credit1 = new Credit();
        credit1.setCreditName("First credit");

        credit2 = new Credit();
        credit2.setCreditName("First credit");

        credit3 = new Credit();
        credit3.setCreditName("First credit");

        credit4 = new Credit();
        credit4.setCreditName("First credit");

        credit5 = new Credit();
        credit5.setCreditName("First credit");
    }
    @Test
    void saveTest() {
        assertNotNull(dao);
        int i =dao.getNewCreditId();
        assertNotEquals(0,i);
        Optional<Credit> result = dao.save(credit1);
        assertFalse(result.isPresent());
        credit1.setId(i);
        result = dao.save(credit1);
        assertTrue(result.isPresent());
        assertEquals(result.get(), credit1);
        assertNotNull(result.get().getId());
        assertTrue(result.get().getId() > 0);
        assertEquals(result.get(), credit1);
        assertNotEquals(result.get(), credit2);
        assertNotEquals(result.get(), credit3);
        assertNotEquals(result.get(), credit4);
        assertNotEquals(result.get(), credit5);
    }
    @Test
    void getAllTest() {
        assertNotNull(dao);
        credit2.setId(dao.getNewCreditId());
        credit4.setId(dao.getNewCreditId());
        assertNotNull(credit2.getId());
        assertNotNull(credit4.getId());
        assertTrue(dao.save(credit2).isPresent());
        assertTrue(dao.save(credit4).isPresent());
        Optional<List<Credit>> list = dao.getAll();
        assertTrue(list.isPresent());
        assertEquals(2, list.get().size());
        assertFalse(list.get().contains(credit1));
        assertTrue(list.get().contains(credit2));
        assertFalse(list.get().contains(credit3));
        assertTrue(list.get().contains(credit4));
        assertFalse(list.get().contains(credit5));
    }
}