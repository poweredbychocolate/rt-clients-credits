package credit;

import credit.model.Sources;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.sql.DataSource;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@SpringBootApplication
@EnableSwagger2
public class CreditServiceApplication {
    private static String dbURL;
    private static String user;
    private static String pass;

    public static void main(String[] args) {
        if(args.length==5) {
            dbURL = args[0];
            user = args[1];
            pass = args[2];
            Sources.setProductUrl(args[3]);
            Sources.setCustomerUrl(args[4]);
            SpringApplication.run(CreditServiceApplication.class, args);
        }else {
            System.out.println("[CreditServiceApplication] args <db address> <user name> <user password> <product address> <customer address> not entered");
        }
    }

    @Bean
    public Docket naughtyApi() {
        return new Docket
                (DocumentationType.SWAGGER_2)
                .select().apis(RequestHandlerSelectors.basePackage("credit.controllers"))
                .paths(PathSelectors.any()).build();
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
    @Bean
    public ExecutorService executorService(){
        return Executors.newCachedThreadPool();
    }

    @Bean
    public DataSource getDataSource(){
        DataSourceBuilder dataSourceBuilder =DataSourceBuilder.create();
        dataSourceBuilder.username(user);
        dataSourceBuilder.password(pass);
        dataSourceBuilder.url(dbURL);
        return dataSourceBuilder.build();
    }
}
