package credit.controllers;

import credit.daos.CreditDAO;
import credit.daos.RestDAO;
import credit.model.CreditInfo;
import model.Credit;
import model.Customer;
import model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * Credit rest controller.
 */
@RestController
public class CreditRest {
    @Autowired
    private CreditDAO dao;
    @Autowired
    private RestDAO restDAO;
    @Autowired
    private ExecutorService executor;
    private final ResponseEntity<Integer> BAD_REQUEST = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    private final ResponseEntity<List<CreditInfo>> NOT_FOUND_LIST = new ResponseEntity<>(HttpStatus.NOT_FOUND);

    /**
     * Create new credit.
     *
     * @param creditInfo {@link CreditInfo} template
     * @return the response entity
     */
    @PostMapping
    ResponseEntity<Integer> createCredit(@RequestBody CreditInfo creditInfo) {
        try {
            int i = dao.getNewCreditId();
            creditInfo.getCredit().setId(i);
            creditInfo.getCustomer().setCreditId(i);
            creditInfo.getProduct().setCreditId(i);
            ///
            Optional<Product> productResponse = restDAO.callCreate(creditInfo.getProduct());
            if (productResponse.isPresent()) {
                creditInfo.setProduct(productResponse.get());
                Optional<Customer> customerResponse = restDAO.callCreate(creditInfo.getCustomer());
                if (customerResponse.isPresent()) {
                    creditInfo.setCustomer(customerResponse.get());
                    Optional<Credit> optional = dao.save(creditInfo.getCredit());
                    if (optional.isPresent()) {
                        return new ResponseEntity<>(optional.get().getId(), HttpStatus.OK);
                    } else {
                        System.out.println("Not implemented yet");
    //TODO remove product and credit by id
                    }
                } else {
                    System.out.println("Not implemented yet");
    //TODO remove product by id
                }
            }
        } catch (Exception e) {
            System.out.println(" ");
        }
        ///
        return BAD_REQUEST;
    }

    /**
     * Gets all credits.
     *
     * @return the credits
     * @throws ExecutionException   the execution exception
     * @throws InterruptedException the interrupted exception
     */
    @GetMapping
    ResponseEntity<List<CreditInfo>> getCredits() throws ExecutionException, InterruptedException {
        Optional<List<Credit>> credits = dao.getAll();
        if (credits.isPresent()) {
            Integer[] creditsId = credits.get().parallelStream().map(Credit::getId).toArray(Integer[]::new);
            Future<Optional<List<Product>>> productFuture = executor.submit(new ProductTask(creditsId));
            Future<Optional<List<Customer>>> customerFuture = executor.submit(new CustomerTask(creditsId));
            Map<Integer, CreditInfo> creditInfos = credits.get().parallelStream().map(CreditInfo::new).collect(Collectors.toMap(creditInfo -> creditInfo.getCredit().getId(), creditInfo -> creditInfo));

            while (!productFuture.isDone() || !customerFuture.isDone()) {
            }

            Optional<List<Customer>> customers = customerFuture.get();
            Optional<List<Product>> products = productFuture.get();

            products.ifPresent(productList -> productList.forEach(product -> {
                creditInfos.get(product.getCreditId()).setProduct(product);
            }));
            customers.ifPresent(customerList -> customerList.forEach(customer -> {
                creditInfos.get(customer.getCreditId()).setCustomer(customer);
            }));
            return new ResponseEntity<>(new ArrayList<>(creditInfos.values()), HttpStatus.OK);
        }
        return NOT_FOUND_LIST;
    }

    /**
     * ProductTask fetch products by use {@link Future}
     */
    private class ProductTask implements Callable<Optional<List<Product>>> {
        private Integer[] ids;

        /**
         * Instantiates a new Product task.
         *
         * @param ids the ids
         */
        public ProductTask(Integer[] ids) {
            this.ids = ids;
        }

        @Override
        public Optional<List<Product>> call() throws Exception {
            return restDAO.callGetProducts(ids);
        }
    }

    /**
     * CustomerTask fetch customers by use {@link Future}
     */
    private class CustomerTask implements Callable<Optional<List<Customer>>> {
        private Integer[] ids;

        /**
         * Instantiates a new Customer task.
         *
         * @param ids the ids
         */
        public CustomerTask(Integer[] ids) {
            this.ids = ids;
        }

        @Override
        public Optional<List<Customer>> call() throws Exception {
            return restDAO.callGetCustomers(ids);
        }
    }

}
