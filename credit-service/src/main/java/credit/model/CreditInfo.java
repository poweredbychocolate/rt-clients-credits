package credit.model;

import model.Credit;
import model.Customer;
import model.Product;

import java.util.Objects;

/**
 * CreditInfo template for rest controller.
 *
 * @author Dawid
 * @version 1
 */
public class CreditInfo {
    private Product product;
    private Customer customer;
    private Credit credit;

    /**
     * Instantiates a new Credit info.
     */
    public CreditInfo() {
    }

    /**
     * Instantiates a new Credit info.
     *
     * @param credit the credit
     */
    public CreditInfo(Credit credit) {
        this.credit = credit;
    }

    /**
     * Instantiates a new Credit info.
     *
     * @param product  the product
     * @param customer the customer
     * @param credit   the credit
     */
    public CreditInfo(Product product, Customer customer, Credit credit) {
        this.product = product;
        this.customer = customer;
        this.credit = credit;
    }

    /**
     * Gets credit.
     *
     * @return the credit
     */
    public Credit getCredit() {
        return credit;
    }

    /**
     * Sets credit.
     *
     * @param credit the credit
     */
    public void setCredit(Credit credit) {
        this.credit = credit;
    }

    /**
     * Gets product.
     *
     * @return the product
     */
    public Product getProduct() {
        return product;
    }

    /**
     * Sets product.
     *
     * @param product the product
     */
    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     * Gets customer.
     *
     * @return the customer
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * Sets customer.
     *
     * @param customer the customer
     */
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreditInfo that = (CreditInfo) o;
        return Objects.equals(product, that.product) &&
                Objects.equals(customer, that.customer) &&
                Objects.equals(credit, that.credit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(product, customer, credit);
    }

    @Override
    public String toString() {
        return "CreditInfo{" +
                "product=" + product +
                ", customer=" + customer +
                ", credit=" + credit +
                '}';
    }
}
