package credit.model;

/**
 * The type Sources, store product service url, customer service url.
 * @author Dawid
 * @version 1
 */
public class Sources {
    private static String productUrl;
    private static String customerUrl;

    /**
     * Gets product url.
     *
     * @return the product url
     */
    public static String getProductUrl() {
        return productUrl;
    }

    /**
     * Sets product url.
     *
     * @param productUrl the product url
     */
    public static void setProductUrl(String productUrl) {
        Sources.productUrl = productUrl;
    }

    /**
     * Gets customer url.
     *
     * @return the customer url
     */
    public static String getCustomerUrl() {
        return customerUrl;
    }

    /**
     * Sets customer url.
     *
     * @param customerUrl the customer url
     */
    public static void setCustomerUrl(String customerUrl) {
        Sources.customerUrl = customerUrl;
    }
}
