package credit.daos;

import credit.model.Sources;
import model.Customer;
import model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Rest Data Access Object, call components rest method and fetch data.
 *
 * @author Dawid
 * @version 1
 */
@Service
public class RestDAO {
    @Autowired
    private RestTemplate restTemplate;

    /**
     * Call create new {@link Product}
     *
     * @param product the product
     * @return the optional created product
     */
    public Optional<Product> callCreate(Product product) {
        try {
            return Optional.ofNullable(restTemplate.postForObject(Sources.getProductUrl(), product, Product.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    /**
     * Call create new {@link Customer}
     *
     * @param customer the customer
     * @return the optional created customer
     */
    public Optional<Customer> callCreate(Customer customer) {
        try {
            return Optional.ofNullable(restTemplate.postForObject(Sources.getCustomerUrl(), customer, Customer.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    /**
     * Call get customers by creditsIds[].
     *
     * @param ids array of {@link Customer#getCreditId()}
     * @return the optional list matched entries
     */
    public Optional<List<Customer>> callGetCustomers(Integer[] ids) {
        try {
            ResponseEntity<Customer[]> responseEntity = restTemplate.exchange(Sources.getCustomerUrl(), HttpMethod.PUT, new HttpEntity<>(ids), Customer[].class);
            if (responseEntity.getBody() != null) return Optional.of(Arrays.asList(responseEntity.getBody()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    /**
     * Call get products by creditsIds[].
     *
     * @param ids array of {@link Product#getCreditId()}
     * @return the optional list matched entries
     */
    public Optional<List<Product>> callGetProducts(Integer[] ids) {
        try {
            ResponseEntity<List<Product>> responseEntity = restTemplate.exchange(Sources.getProductUrl(), HttpMethod.PUT, new HttpEntity<>(ids), new ParameterizedTypeReference<List<Product>>() {
            });
            if (responseEntity.getBody() != null) return Optional.of(responseEntity.getBody());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }
}
