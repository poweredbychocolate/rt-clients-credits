package credit.daos;

import model.Credit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Credit data access object.
 *
 * @author Dawid
 * @version 1
 */
@Service
public class CreditDAO {
    @Autowired
    private JdbcTemplate template;
    private AtomicInteger creditKey;

    @PostConstruct
    private void onInit() {
        template.execute("CREATE SCHEMA IF NOT EXISTS CreditDB");
        template.execute("CREATE TABLE IF NOT EXISTS CreditDB.Credit(" +
                "Id INT NOT NULL, CreditName VARCHAR(500) NOT NULL," +
                "PRIMARY KEY(Id))");
        int i;
        try {
            i = template.queryForObject("SELECT MAX(c.Id) FROM CreditDB.Credit c", Integer.class);
        } catch (Exception e) {
            i = 0;
        }
        creditKey = new AtomicInteger(++i);
        System.out.println("[CreditDAO] Loaded Key : " + creditKey.get());
        System.out.println("[CreditDAO] Init Successfully (๑꧆◡꧆๑)");
    }

    /**
     * Gets generated id for credit.
     *
     * @return the new credit id
     */
    public int getNewCreditId() {
        return creditKey.getAndIncrement();
    }

    /**
     * Save new credit.
     *
     * @param credit the credit
     * @return saved credit as optional
     */
    public Optional<Credit> save(Credit credit) {
        try {
            int i = save(credit.getId(), credit.getCreditName());
            if (i > 0) {
                return Optional.of(credit);
            }
        } catch (Exception e) {
            System.out.println("[CreditDAO] cannot save entity");
        }
        return Optional.empty();
    }

    /**
     * Save credit row.
     *
     * @param id         the id
     * @param creditName the credit name
     * @return the int
     */
    public int save(int id, String creditName) {
        final String insertSql = "INSERT INTO CreditDB.Credit(Id, CreditName) VALUES (?,?)";
        int c = 0;
        try {
            c = template.update(connection -> {
                PreparedStatement statement = connection.prepareStatement(insertSql);
                statement.setInt(1, id);
                statement.setString(2, creditName);
                return statement;
            });
        } catch (DataAccessException e) {
            System.out.println("[CreditDAO] cannot save row");
        }
        return c;
    }

    /**
     * Gets all credits.
     *
     * @return the all
     */
    public Optional<List<Credit>> getAll() {
        List<Credit> list = template.query("SELECT Id,CreditName FROM CreditDB.Credit", (resultSet, i) -> {
            Credit credit = new Credit();
            credit.setId(resultSet.getInt("Id"));
            credit.setCreditName(resultSet.getString("CreditName"));
            return credit;
        });
        return list.isEmpty() ? Optional.empty() : Optional.of(list);
    }
}
