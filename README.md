#Clients Credits project
-Spring Boot

#How to run docker cointainers
#database-service
cd creditModel
mvn clean install
docker run -d -p 14141:3306 clients-credits/database-service:0.1  --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci

#product-service
cd product-service
mvn clean install
docker run -p 16161:8080 -d clients-credits/product-service:0.1 "jdbc:mariadb://192.168.200.10:14141" "credit" "credit"

#customer-service
cd customer-service
mvn clean install
docker run -p 16162:8080 -d clients-credits/customer-service:0.1 "jdbc:mariadb://192.168.200.10:14141" "credit" "credit"

#credit-service
cd customer-service
mvn clean install
docker run -p 8080:8080 -d clients-credits/credit-service:0.1 "jdbc:mariadb://192.168.200.10:14141" "credit" "credit" "http://192.168.200.79:16161" "http://192.168.200.79:16162"