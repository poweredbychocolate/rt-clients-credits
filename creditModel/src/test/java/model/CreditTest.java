package model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Credit test.
 * @author Dawid
 * @version 1
 */
class CreditTest {

    /**
     * Test credit.
     */
    @Test
    void testCredit(){
        Credit credit = new Credit();
        Integer i = 345;
        String m ="Test Credit";
        assertNotNull(credit);
        credit.setId(i);
        credit.setCreditName(m);
        assertEquals(i,credit.getId());
        assertEquals(m,credit.getCreditName());
    }
}