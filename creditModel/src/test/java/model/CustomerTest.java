package model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Customer test.
 * @author Dawid
 * @version 1
 */
class CustomerTest {
    /**
     * Test customer.
     */
    @Test
    void testCustomer(){
        Customer customer = new Customer();
        Integer i = 345, cId=698;
        String f ="Test First Name";
        String s ="Test Surname";
        String p ="Test Pesel";
        assertNotNull(customer);

        customer.setId(i);
        customer.setCreditId(cId);
        customer.setPesel(p);
        customer.setSurname(s);
        customer.setFirstName(f);

        assertEquals(i,customer.getId());
        assertEquals(cId,customer.getCreditId());
        assertEquals(f,customer.getFirstName());
        assertEquals(s,customer.getSurname());
        assertEquals(p,customer.getPesel());
    }

}