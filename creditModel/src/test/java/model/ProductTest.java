package model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Product test.
 * @author Dawid
 * @version 1
 */
class ProductTest {
    /**
     * Test product.
     */
    @Test
    void testProduct() {
        Product product = new Product();
        Integer i = 345, cId = 2658, v = 158;
        String m = "Test Credit";
        assertNotNull(product);
        product.setId(i);
        product.setCreditId(cId);
        product.setProductName(m);
        product.setValue(v);
        assertEquals(i, product.getId());
        assertEquals(cId, product.getCreditId());
        assertEquals(v, product.getValue());
        assertEquals(m, product.getProductName());
    }

}