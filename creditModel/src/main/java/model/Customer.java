package model;

import java.util.Objects;

/**
 * The type Customer.
 *
 * @author Dawid
 * @version 1
 * @since 2020-02-21
 */
public class Customer {
    private Integer id;
    private Integer creditId;
    private String firstName;
    private String surname;
    private String pesel;

    /**
     * Instantiates a new Customer.
     */
    public Customer() {
    }

    /**
     * Instantiates a new Customer.
     *
     * @param id        the id
     * @param firstName the first name
     * @param surname   the surname
     * @param pesel     the pesel
     */
    public Customer(Integer id, String firstName, String surname, String pesel) {
        this.id = id;
        this.firstName = firstName;
        this.surname = surname;
        this.pesel = pesel;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets credit id.
     *
     * @return the credit id
     */
    public Integer getCreditId() {
        return creditId;
    }

    /**
     * Sets credit id.
     *
     * @param creditId the credit id
     */
    public void setCreditId(Integer creditId) {
        this.creditId = creditId;
    }

    /**
     * Gets first name.
     *
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets first name.
     *
     * @param firstName the first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets surname.
     *
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets surname.
     *
     * @param surname the surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Gets pesel.
     *
     * @return the pesel
     */
    public String getPesel() {
        return pesel;
    }

    /**
     * Sets pesel.
     *
     * @param pesel the pesel
     */
    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(id, customer.id) &&
                Objects.equals(creditId, customer.creditId) &&
                Objects.equals(firstName, customer.firstName) &&
                Objects.equals(surname, customer.surname) &&
                Objects.equals(pesel, customer.pesel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, creditId, firstName, surname, pesel);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", creditId=" + creditId +
                ", firstName='" + firstName + '\'' +
                ", surname='" + surname + '\'' +
                ", pesel='" + pesel + '\'' +
                '}';
    }
}
