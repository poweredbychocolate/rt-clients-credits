package model;

import java.util.Objects;

/**
 * The type Product.
 *
 * @author Dawid
 * @version 1
 * @since 2020-02-21
 */
public class Product {
    private Integer id;
    private Integer creditId;
    private Integer value;
    private String productName;

    /**
     * Instantiates a new Product.
     */
    public Product() {
    }

    /**
     * Instantiates a new Product.
     *
     * @param id          the id
     * @param creditId    the credit id
     * @param value       the value
     * @param productName the product name
     */
    public Product(Integer id, Integer creditId, Integer value, String productName) {
        this.id = id;
        this.creditId = creditId;
        this.value = value;
        this.productName = productName;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets credit id.
     *
     * @return the credit id
     */
    public Integer getCreditId() {
        return creditId;
    }

    /**
     * Sets credit id.
     *
     * @param creditId the credit id
     */
    public void setCreditId(Integer creditId) {
        this.creditId = creditId;
    }

    /**
     * Gets value.
     *
     * @return the value
     */
    public Integer getValue() {
        return value;
    }

    /**
     * Sets value.
     *
     * @param value the value
     */
    public void setValue(Integer value) {
        this.value = value;
    }

    /**
     * Gets product name.
     *
     * @return the product name
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Sets product name.
     *
     * @param productName the product name
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(id, product.id) &&
                Objects.equals(creditId, product.creditId) &&
                Objects.equals(value, product.value) &&
                Objects.equals(productName, product.productName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, creditId, value, productName);
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", creditId=" + creditId +
                ", value=" + value +
                ", productName='" + productName + '\'' +
                '}';
    }
}
