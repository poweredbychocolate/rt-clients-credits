package model;

import java.util.Objects;

/**
 * The type Credit.
 *
 * @author Dawid
 * @version 1
 * @since 2020-02-21
 */
public class Credit {
    private Integer id;
    private String creditName;

    /**
     * Instantiates a new Credit.
     */
    public Credit() {
    }

    /**
     * Instantiates a new Credit.
     *
     * @param id         the id
     * @param creditName the credit name
     */
    public Credit(Integer id, String creditName) {
        this.id = id;
        this.creditName = creditName;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets credit name.
     *
     * @return the credit name
     */
    public String getCreditName() {
        return creditName;
    }

    /**
     * Sets credit name.
     *
     * @param creditName the credit name
     */
    public void setCreditName(String creditName) {
        this.creditName = creditName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Credit credit = (Credit) o;
        return Objects.equals(id, credit.id) &&
                Objects.equals(creditName, credit.creditName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, creditName);
    }

    @Override
    public String toString() {
        return "Credit{" +
                "id=" + id +
                ", creditName='" + creditName + '\'' +
                '}';
    }
}
