CREATE SCHEMA IF NOT EXISTS CreditDB;
CREATE SCHEMA IF NOT EXISTS ProductDB;
CREATE SCHEMA IF NOT EXISTS CustomerDB;

SHOW DATABASES;

CREATE USER IF NOT EXISTS 'credit'@'%' IDENTIFIED BY 'credit';
GRANT USAGE ON *.* TO 'credit'@'%';

GRANT EXECUTE, SELECT, SHOW VIEW, ALTER, ALTER ROUTINE, CREATE, CREATE ROUTINE, CREATE TEMPORARY TABLES, CREATE VIEW, DELETE, DROP, EVENT, INDEX, INSERT, REFERENCES, TRIGGER, UPDATE, LOCK TABLES  ON `CreditDB`.* TO 'credit'@'%' WITH GRANT OPTION;
GRANT EXECUTE, SELECT, SHOW VIEW, ALTER, ALTER ROUTINE, CREATE, CREATE ROUTINE, CREATE TEMPORARY TABLES, CREATE VIEW, DELETE, DROP, EVENT, INDEX, INSERT, REFERENCES, TRIGGER, UPDATE, LOCK TABLES  ON `CustomerDB`.* TO 'credit'@'%' WITH GRANT OPTION;
GRANT EXECUTE, SELECT, SHOW VIEW, ALTER, ALTER ROUTINE, CREATE, CREATE ROUTINE, CREATE TEMPORARY TABLES, CREATE VIEW, DELETE, DROP, EVENT, INDEX, INSERT, REFERENCES, TRIGGER, UPDATE, LOCK TABLES  ON `ProductDB`.* TO 'credit'@'%' WITH GRANT OPTION;

FLUSH PRIVILEGES;
SHOW GRANTS FOR 'credit'@'%';

USE ProductDB;
CREATE TABLE IF NOT EXISTS `Product` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	`CreditId` INT NOT NULL,
	`ProductName` VARCHAR(500) NOT NULL,
	`Value` INT NOT NULL,
	PRIMARY KEY (`Id`)
);

USE CustomerDB;
CREATE TABLE IF NOT EXISTS `Customer` (
	`Id` INT NOT NULL AUTO_INCREMENT,
	`CreditId` INT NOT NULL,
	`FirstName` VARCHAR(500) NOT NULL,
	`Pesel` VARCHAR(11) NOT NULL,
	`Surname` VARCHAR(500) NOT NULL,
	PRIMARY KEY (`Id`)
);

USE CreditDB;
CREATE TABLE IF NOT EXISTS `Credit`(
	`Id` INT NOT NULL ,
	`CreditName` VARCHAR(500) NOT NULL,
	PRIMARY KEY(`Id`)
);

