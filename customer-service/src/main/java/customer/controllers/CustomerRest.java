package customer.controllers;

import customer.daos.CustomerDAO;
import model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * Customer rest controller.
 *
 * @author Dawid
 * @version 1
 */
@RestController
public class CustomerRest {
    @Autowired
    private CustomerDAO dao;

    private final ResponseEntity<Customer> CONFLICT = new ResponseEntity<>(HttpStatus.CONFLICT);
    private final ResponseEntity<List<Customer>> NOT_FOUND_LIST = new ResponseEntity<>(HttpStatus.NOT_FOUND);

    /**
     * Create new {@link Customer}
     *
     * @param customer the customer
     * @return the Customer entity or code 409
     */
    @PostMapping
    synchronized ResponseEntity<Customer> createCustomer(@RequestBody Customer customer) {
        try {
            Optional<Customer> tmp = dao.save(customer);
            if (tmp.isPresent()) return new ResponseEntity<>(tmp.get(), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return CONFLICT;
    }

    /**
     * Gets {@link Customer} list that has id on ids list
     *
     * @param ids the ids list
     * @return the founded products
     */
    @PutMapping
    ResponseEntity<List<Customer>> getCustomers(@RequestBody Integer[] ids) {
        Optional<List<Customer>> list = dao.getAll(ids);
        return list.map(products -> new ResponseEntity<>(products, HttpStatus.OK)).orElse(NOT_FOUND_LIST);
    }
}
