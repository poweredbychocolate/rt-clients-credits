package customer.daos;

import model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;

/**
 * Customer Data Access Object.
 */
@Service
public class CustomerDAO {
    @Autowired
    private JdbcTemplate template;

    @PostConstruct
    private void onInit() {
        template.execute("CREATE SCHEMA IF NOT EXISTS CustomerDB");
        template.execute("CREATE TABLE IF NOT EXISTS CustomerDB.Customer(" +
                "Id INT NOT NULL AUTO_INCREMENT, CreditId INT NOT NULL," +
                "FirstName VARCHAR(500) NOT NULL, Pesel VARCHAR(11) NOT NULL, " +
                "Surname VARCHAR(500) NOT NULL, PRIMARY KEY (`Id`))");
        System.out.println("[CustomerDAO] Init Successfully (๑꧆◡꧆๑)");
    }

    /**
     * Save customer row.
     *
     * @param creditId  the credit id
     * @param pesel     the pesel
     * @param firstName the first name
     * @param surname   the surname
     * @return generated customer id
     */
    public Integer save(int creditId, String pesel, String firstName, String surname) {
        final String insertSql = "INSERT INTO CustomerDB.Customer (CreditId, FirstName, Pesel,Surname) VALUES (?,?,?,?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        int count = template.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(insertSql, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, creditId);
            ps.setString(2, firstName);
            ps.setString(3, pesel);
            ps.setString(4, surname);
            return ps;
        }, keyHolder);
        return count > 0 && keyHolder.getKey() != null ? keyHolder.getKey().intValue() : null;
    }

    /**
     * Save {@link Customer}
     *
     * @param customer the customer to save
     * @return the saved customer with id
     */
    public Optional<Customer> save(Customer customer) {
        Integer i = save(customer.getCreditId(), customer.getPesel(), customer.getFirstName(), customer.getSurname());
        if (i != null) {
            customer.setId(i);
            return Optional.of(customer);
        }
        return Optional.empty();
    }

    /**
     * Gets {@link Customer} list that has id on ids list
     *
     * @param creditIds the credit ids
     * @return the customers list
     */
    public Optional<List<Customer>> getAll(Integer[] creditIds) {
        StringBuilder stringBuilder = new StringBuilder("SELECT * FROM CustomerDB.Customer c WHERE c.CreditId IN(");
        for (Integer i : creditIds) {
            stringBuilder.append(i.toString());
            stringBuilder.append(", ");
        }
        stringBuilder.delete(stringBuilder.length() - 2, stringBuilder.length());
        stringBuilder.append(")");

        List<Customer> list = template.query(stringBuilder.toString(), (resultSet, i) -> {
            Customer customer = new Customer();
            customer.setId(resultSet.getInt("Id"));
            customer.setCreditId(resultSet.getInt("CreditId"));
            customer.setFirstName(resultSet.getString("FirstName"));
            customer.setSurname(resultSet.getString("Surname"));
            customer.setPesel(resultSet.getString("Pesel"));
            return customer;
        });
        return list.size() > 0 ? Optional.of(list) : Optional.empty();
    }

}
