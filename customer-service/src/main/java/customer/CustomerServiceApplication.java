package customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.sql.DataSource;

@SpringBootApplication
@EnableSwagger2
public class CustomerServiceApplication {
    private static String dbURL;
    private static String user;
    private static String pass;

    public static void main(String[] args) {
        if (args.length == 3) {
            dbURL = args[0];
            user = args[1];
            pass = args[2];
            SpringApplication.run(CustomerServiceApplication.class, args);
        }else {
            System.out.println("[CustomerServiceApplication] args <db address> <user name> <user password> not entered");
        }
    }
    @Bean
    public Docket naughtyApi() {
        return new Docket
                (DocumentationType.SWAGGER_2)
                .select().apis(RequestHandlerSelectors.basePackage("customer.controllers"))
                .paths(PathSelectors.any()).build();
    }
    @Bean
    public DataSource getDataSource(){
        DataSourceBuilder dataSourceBuilder =DataSourceBuilder.create();
        dataSourceBuilder.username(user);
        dataSourceBuilder.password(pass);
        dataSourceBuilder.url(dbURL);
        return dataSourceBuilder.build();
    }
}
