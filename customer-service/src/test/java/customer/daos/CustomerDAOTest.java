package customer.daos;

import model.Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.sql.DataSource;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Customer dao test.
 *
 * @author Dawid
 * @version 1
 */
@ExtendWith(SpringExtension.class)
@JdbcTest
@ComponentScan
class CustomerDAOTest {

    @Autowired
    private CustomerDAO dao;
    @Autowired
    @Qualifier("embeddedDB")
    private DataSource dataSource;
    /**
     * The Template.
     */
    @Autowired
    JdbcTemplate template;

    private Customer customer1, customer2, customer3, customer4, customer5;

    /**
     * Before each.
     */
    @BeforeEach
    void beforeEach() {
        customer1 = new Customer();
        customer1.setFirstName("Test 1");
        customer1.setSurname("Surname 1");
        customer1.setPesel("74859663524");
        customer1.setCreditId(50);

        customer2 = new Customer();
        customer2.setFirstName("Test 2");
        customer2.setSurname("Surname 2");
        customer2.setPesel("25589656142");
        customer2.setCreditId(51);

        customer3 = new Customer();
        customer3.setFirstName("Test 3");
        customer3.setSurname("Surname 3");
        customer3.setPesel("74851447582");
        customer3.setCreditId(52);

        customer4 = new Customer();
        customer4.setFirstName("Test 4");
        customer4.setSurname("Surname 4");
        customer4.setPesel("56324866248");
        customer4.setCreditId(53);

        customer5 = new Customer();
        customer5.setFirstName("Test 5");
        customer5.setSurname("Surname 5");
        customer5.setPesel("45612378951");
        customer5.setCreditId(54);

    }

    /**
     * Save test.
     */
    @Test
    void saveTest() {
        assertNotNull(dao);
        Optional<Customer> result = dao.save(customer1);
        assertTrue(result.isPresent());
        assertEquals(result.get(), customer1);
        assertNotNull(result.get().getId());
        assertTrue(result.get().getId() > 0);
        assertEquals(result.get(), customer1);
        assertNotEquals(result.get(), customer2);
        assertNotEquals(result.get(), customer3);
        assertNotEquals(result.get(), customer4);
        assertNotEquals(result.get(), customer5);
    }

    /**
     * Gets all test.
     */
    @Test
    void getAllTest() {
        assertNotNull(dao);
        assertTrue(dao.save(customer2).isPresent());
        assertTrue(dao.save(customer4).isPresent());
        Optional<List<Customer>> list = dao.getAll(new Integer[]{3000, customer2.getCreditId(), 3001});
        assertTrue(list.isPresent());
        assertEquals(1, list.get().size());
        assertFalse(list.get().contains(customer1));
        assertTrue(list.get().contains(customer2));
        assertFalse(list.get().contains(customer3));
        list = dao.getAll(new Integer[]{3000, 3001, customer2.getCreditId(), 4580, 58955, customer4.getCreditId(), 300, 302});
        assertTrue(list.isPresent());
        assertEquals(2, list.get().size());
        assertFalse(list.get().contains(customer1));
        assertTrue(list.get().contains(customer2));
        assertFalse(list.get().contains(customer3));
        assertTrue(list.get().contains(customer4));
        assertFalse(list.get().contains(customer5));
    }
}