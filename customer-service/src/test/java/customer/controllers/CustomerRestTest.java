package customer.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import customer.daos.CustomerDAO;
import model.Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

/**
 * The type Customer rest test.
 * @author Dawid
 * @version 1
 */
@WebMvcTest(CustomerRest.class)
class CustomerRestTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private CustomerDAO customerDAO;

    private Map<Integer, Customer> map;
    private int index;

    /**
     * Before each.
     */
    @BeforeEach
    void beforeEach() {
        map = new HashMap<>(5);
        index = 1;
    }

    /**
     * Product init.
     */
    void customerInit() {
        Customer customer = new Customer();
        customer.setId(index++);
        customer.setFirstName("Test 1");
        customer.setSurname("Surname 1");
        customer.setPesel("74859663524");
        customer.setCreditId(50);
        map.put(customer.getId(), customer);

        customer = new Customer();
        customer.setId(index++);
        customer.setFirstName("Test 2");
        customer.setSurname("Surname 2");
        customer.setPesel("25589656142");
        customer.setCreditId(51);
        map.put(customer.getId(), customer);

        customer = new Customer();
        customer.setId(index++);
        customer.setFirstName("Test 3");
        customer.setSurname("Surname 3");
        customer.setPesel("74851447582");
        customer.setCreditId(52);
        map.put(customer.getId(), customer);

        customer = new Customer();
        customer.setId(index++);
        customer.setFirstName("Test 4");
        customer.setSurname("Surname 4");
        customer.setPesel("56324866248");
        customer.setCreditId(53);
        map.put(customer.getId(), customer);

        customer = new Customer();
        customer.setId(index++);
        customer.setFirstName("Test 5");
        customer.setSurname("Surname 5");
        customer.setPesel("45612378951");
        customer.setCreditId(54);
        map.put(customer.getId(), customer);

        when(customerDAO.getAll(any(Integer[].class))).thenAnswer(invocationOnMock -> {
            Integer[] keys = invocationOnMock.getArgument(0);
            List<Customer> list = new ArrayList<>();
            for (Integer key : keys) {
                list.addAll(map.values().stream().filter(p -> p.getCreditId().equals(key)).collect(Collectors.toList()));
            }
            return list.size() > 0 ? Optional.of(list) : Optional.empty();
        });
    }

    /**
     * Gets customers test.
     *
     * @throws Exception the exception
     */
    @Test
    void getCustomersTest() throws Exception {
        customerInit();
        mockMvc.perform(MockMvcRequestBuilders.put("/")
                .content("[2000,20001,200,234,443,345]").contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/")
                .content("[2000,50,200,54,234,443,345,52]").contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[1]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[2]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[3]").doesNotExist()).andReturn();
        ObjectMapper mapper = new JsonMapper();
        List<Customer> list = Arrays.asList(mapper.readValue(mvcResult.getResponse().getContentAsString(), Customer[].class));
        assertNotNull(list);
        assertTrue(list.contains(map.get(1)));
        assertFalse(list.contains(map.get(2)));
        assertTrue(list.contains(map.get(3)));
        assertFalse(list.contains(map.get(4)));
        assertTrue(list.contains(map.get(5)));
    }

    /**
     * Create product.
     *
     * @throws Exception the exception
     */
    @Test
    void createProduct() throws Exception {
        when(customerDAO.save(any(Customer.class))).thenAnswer(invocationOnMock -> {
            Customer customer = invocationOnMock.getArgument(0);
            customer.setId(customerDAO.save(customer.getCreditId(), customer.getPesel(), customer.getFirstName(), customer.getSurname()));
            map.put(customer.getId(), customer);
            return Optional.of(customer);
        });

        when(customerDAO.save(anyInt(), anyString(), anyString(), anyString())).thenReturn(index++);

        Customer customer = new Customer();
        customer.setFirstName("Local Test ");
        customer.setSurname("Surname Test");
        customer.setPesel("25589655942");
        customer.setCreditId(326);
        ObjectMapper mapper = new JsonMapper();
        mockMvc.perform(MockMvcRequestBuilders.post("/").content(mapper.writeValueAsString(customer))
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.creditId").value(customer.getCreditId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value(customer.getFirstName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.pesel").value(customer.getPesel()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.surname").value(customer.getSurname()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber());

    }
}